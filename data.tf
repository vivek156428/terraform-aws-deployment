data "aws_route53_zone" "houston" {
  zone_id = "${var.zone_id}"
  private_zone = false
}

data "aws_caller_identity" "current" {}
data "aws_ami" "ami" {
  most_recent = true
  owners = ["${data.aws_caller_identity.current.account_id}"]

  filter {
    name = "name"
    values = ["EYGIT-awslinux-server*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}


data "aws_ami" "ecs_ami" {
  most_recent = true
  owners = ["${data.aws_caller_identity.current.account_id}"]

  filter {
    name = "name"
    values = ["EYGIT-awslinux-ecs*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}


data "aws_region" "current" {}


data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["099720109477"]

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

