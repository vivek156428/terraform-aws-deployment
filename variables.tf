variable "deployment_name"
{
  type = "string"
  description = "the deployment name (used in object name tags, etc)"
}


variable "chargecode"
{
  type = "string"
  description = "EY chargecode"
}


variable "consumer"
{
  type = "string"
  description = "Consumer(s) of the deployment (comma separated)"
}


variable "techcontacts"
{
  type = "string"
  description = "Technical contact for deployment issues"
}


variable "extra_tags"
{
  default = {}
  type = "map"
  description = "Map of extra tags to apply to every taggable object"
}


variable "zone_id"
{
  type = "string"
  description = "AWS domain ID of the Route53 domain to use for DNS records"
}


variable "orion_base_url"
{
  type = "string"
  description = "the associated storage manager (orion) base URL"
}


variable "ami_id"
{
  default = ""
  type = "string"
  description = "CID of reference AMI to use for non-ECS instances"
}


variable "ecs_ami_id"
{
  default = ""
  type = "string"
  description = "CID of reference AMI to use for ECS instances"
}


variable "docker_ami_id"
{
  default = ""
  type = "string"
  description = "CID of reference AMI to use for Docker instances"
}


variable "bootstrap_docker_with_ubuntu"
{
  default = ""
  type = "string"
  description = "bootstrap dockervm with Ubuntu script else Amazon Linux script"
}


variable "rds_database_name"
{
  default = "houston"
  type = "string"
  description = "the RDS database name"
}


variable "rds_username"
{
  type = "string"
  description = "the RDS database username"
}


variable "rds_password"
{
  type = "string"
  description = "the RDS database password"
}


variable "rds_instance_count"
{
  default = 1
  type = "string"
  description = "the RDS instance count"
}


variable "rds_instance_type"
{
  default = "db.t2.medium"
  type = "string"
  description = "the RDS instance type"
}

variable "instance_type"
{
  default = "t2.medium"
  type = "string"
  description = "the ECS instance type"
}

variable "subnet_count"
{
  default = "2"
  type = "string"
  description = "number of subnets to create in new VPC or use (via output reference) in external VPC"
}


variable "docker_instance_count"
{
  default = 1
  type = "string"
  description = "number of Docker worker VMs to deploy"
}


variable "rabbit_host"
{
  type = "string"
  description = "RabbitMQ host"
}

variable "basic_auth_username"
{
  type = "string"
  description = "Username for Houston's Basic Auth"
}

variable "basic_auth_password"
{
  type = "string"
  description = "Password for Houston's Basic Auth"
}

variable "authorized_keys"
{
  type = "string"
  description = "content of OpenSSH's authorized_keys for jumpbox"
}


variable "public_key"
{
  default = ""
  type = "string"
  description = "public key to use for deployment SSH key pair (default: auto generate)"
}


variable "data_bucket_sprintf"
{
  default = ""
  type = "string"
  description = "C printf style syntax for generating the bucket name (single %s to receive the current region)"
}


variable "landing_bucket_sprintf"
{
  default = ""
  type = "string"
  description = "C printf style syntax for generating the bucket name (single %s to receive the current region)"
}

variable "endpoint_name_sprintf"
{
  default = ""
  type = "string"
  description = "C printf style syntax for generating the endpoint name (single %s to receive base name)"
}

variable "ecr_region" {
  type = "string"
  description = "region of ECR repos for Houston Workers docker images"
}
variable "job_status_docker_image_url" {
  type = "string"
  description = "url where Job Status Service docker image can be found"
}
variable "job_status_docker_image_tag" {
  type = "string"
  description = "Tag to use for Status Service docker image"
  default = "latest"
}
variable "job_runner_docker_image_url" {
  type = "string"
  description = "url where Job Runner docker image can be found"

}
variable "job_runner_docker_image_tag" {
  type = "string"
  description = "Tag to use for Job Runner docker image"
  default = "latest"
}
variable "db_name" {
  type = "string"
  description = "database name"
  default = "houston"
}

variable "landing_bucket_prefix"
{
  default = "landing_zone/"
  type = "string"
  description = "key prefix for the upload watcher Lambda trigger"
}

variable "is_master" {
  default = ""
  type = "string"
  description = "Specify if you want to do a multi-region deployment with API Gateway, single regions don't use it. Empty means false, any string means true (even if it is false)"
}

variable "api_gateway_docker_image_url" {
  type = "string"
  description = "url where API Gateway docker image can be found"
}

variable "api_gateway_docker_image_tag" {
  type = "string"
  description = "Tag to use for API Gateway docker image"
  default = "latest"
}
variable "rabbitmq_docker_image_url" {
  type = "string"
  default = "rabbitmq"
  description = "url where rabbitmq docker image can be found"
}

variable "rabbitmq_docker_image_tag" {
  type = "string"
  description = "Tag to use for rabbitmq docker image"
  default = "3-management"
}

variable "ey_houston_env_docker_url" {
  type = "string"
  description = "Url to use for Houston Env"
}

variable "ey_houston_env_docker_tag" {
  type = "string"
  description = "Tag to use for Houston Env"
  default = "latest"
}

variable "buckets_access_principles_arn" {
  type = "string"
  description = "principle arn in SM account to grant access to landing and data buckets"
}

variable "min_ecs_task_percent" {
  description = "minimum percent for tasks to have when deployment happens"
  default = "30"
}
variable "max_ecs_task_percent" {
  description = "maximum percent for tasks to have when deployment happens"
  default = "100"
}

variable "cloudwatch_log_retention_in_days" {
  description = "the number of days to keep cloudwatch logs"
  default = "30"
}

variable "db_on_restart" {
  description = "'recreate' value means it will re-create the db upon service restart, default value is to retain the database"
  default = "retain"
}

variable "upload_watcher_s3_bucket" {
  description = "s3 bucket to look for the zip file to run"
  default = ""
}

variable "docker_vm_size" {
  description = "size for the docker vm to have"
  default = "t2.large"
}
variable "production" {
  description = "If it is a production environment or not. saying yes will keep all volumes after ec2 instanceis deleted"
  default = true
}

variable "ecs_instance_count" {
  description = "Number of ecs instances to launch"
  default = 2
}

variable "rabbit_username" {
  default = "smrabbit"
}
variable "rabbit_password" {
  default = "owqeowieudn187"
}