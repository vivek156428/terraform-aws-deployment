module "network"
{
  source = "modules/network"

  deployment_name = "${var.deployment_name}"
  zone_id = "${var.zone_id}"
  subnet_count = "${var.subnet_count}"
  authorized_keys = "${var.authorized_keys}"
  ssh_key_name = "${aws_key_pair.default.key_name}"

  ami_id = "${local.ami_id}"

  deployment_tags = "${local.deployment_tags}"
  production = "${var.production}"
}


module "s3"
{
  source = "modules/s3"

  deployment_name = "${var.deployment_name}"
  data_bucket_name = "${local.data_bucket_name}"
  landing_bucket_name = "${local.landing_bucket_name}"
  principle_arn = "${var.buckets_access_principles_arn}"

  deployment_tags = "${local.deployment_tags}"
  production = "${var.production}"
}


module "upload_watcher"
{
  source = "modules/upload_watcher"

  deployment_name = "${var.deployment_name}"
  landing_bucket_id = "${module.s3.landing_bucket_id}"
  landing_bucket_prefix = "${var.landing_bucket_prefix}"

  env_vars = {
    HOUSTON_BASIC_AUTH = "${var.basic_auth_username}:${var.basic_auth_password}"
    HOUSTON_URL = "${module.job_status_service.endpoint_url}"
  }

  deployment_tags = "${local.deployment_tags}"
  cloudwatch_log_group_name = "${module.cloudwatch_logs.name}"
  s3_bucket = "${var.upload_watcher_s3_bucket}"
}


module "rds"
{
  source = "modules/rds"

  deployment_name = "${var.deployment_name}"
  zone_id = "${var.zone_id}"
  subnet_ids = "${module.network.subnet_ids}"

  rds_database_name = "${var.rds_database_name}"
  rds_username = "${var.rds_username}"
  rds_password = "${var.rds_password}"

  rds_instance_count = "${var.rds_instance_count}"
  rds_instance_type = "${var.rds_instance_type}"

  security_group_ids = ["${module.network.default_security_group_id}"]

  deployment_tags = "${local.deployment_tags}"
  production = "${var.production}"
}

module "iam" {
  source = "modules/iam"
  deployment_name = "${var.deployment_name}"
}


module "ecs_cluster"
{
  source = "modules/ecs"

  deployment_name = "${var.deployment_name}"
  cluster_name = "houston-${var.deployment_name}"
  instance_count = "${var.ecs_instance_count}"
  instance_type = "${var.instance_type}"
  subnet_ids = "${module.network.subnet_ids}"

  block_size = 40
  ssh_key_name = "${aws_key_pair.default.key_name}"

  ami_id = "${local.ecs_ami_id}"
  deployment_tags = "${local.deployment_tags}"
  production = "${var.production}"
}


module "job_status_service"
{
  source = "modules/job_status_service"

  deployment_name = "${var.deployment_name}"
  endpoint_name = "${local.endpoint_name}"
  zone_id = "${var.zone_id}"
  cluster_id = "${module.ecs_cluster.cluster_id}"
  acm_certificate_arn = "${module.acm.acm_arn}"
  subnet_ids = "${module.network.subnet_ids}"

  rabbit_username = "${var.rabbit_username}"
  rabbit_password = "${var.rabbit_password}"
  rabbit_host = "${local.rabbit_host}"

  desired_count = 1
  service_role_id = "${module.ecs_cluster.service_role_id}"

  rds_url = "${module.rds.rds_url}"
  rds_database_name = "${var.rds_database_name}"
  rds_username = "${var.rds_username}"
  rds_password = "${var.rds_password}"

  docker_image_url = "${join("", list(var.job_status_docker_image_url, ":", var.job_status_docker_image_tag))}"

  basic_auth_username = "${var.basic_auth_username}"
  basic_auth_password = "${var.basic_auth_password}"
  env_docker_repo = "${var.ey_houston_env_docker_url}"

  vpc_id = "${module.network.vpc_id}"
  default_security_group_id = "${module.network.default_security_group_id}"

  deployment_tags = "${local.deployment_tags}"
  min_ecs_task_percent = "${var.min_ecs_task_percent}"
  max_ecs_task_percent = "${var.max_ecs_task_percent}"
  cloudwatch_log_group_name = "${module.cloudwatch_logs.name}"
  db_on_restart = "${var.db_on_restart}"
}



module "ecs_job_runner_service"
{
  source = "modules/job_runner_service"

  deployment_name = "${var.deployment_name}"
  zone_id = "${var.zone_id}"
  cluster_id = "${module.ecs_cluster.cluster_id}"
  acm_certificate_arn = "${module.acm.acm_arn}"
  subnet_ids = "${module.network.subnet_ids}"

  rabbit_username = "${var.rabbit_username}"
  rabbit_password = "${var.rabbit_password}"
  rabbit_host = "${local.rabbit_host}"

  desired_count = 1
  instance_role_id = "${module.ecs_cluster.instance_role_id}"
  instance_role_name = "${module.ecs_cluster.instance_role_name}"

  rds_url = "${module.rds.rds_url}"
  rds_database_name = "${var.rds_database_name}"
  rds_username = "${var.rds_username}"
  rds_password = "${var.rds_password}"

  docker_image_url = "${join("", list(var.job_runner_docker_image_url, ":", var.job_runner_docker_image_tag))}"

  ecr_region = "${var.ecr_region}"

  vpc_id = "${module.network.vpc_id}"
  default_security_group_id = "${module.network.default_security_group_id}"

  docker_host = "${module.docker-vm.docker_vm_address}"
  orion_url = "https://${local.orion_url}"
  min_ecs_task_percent = "${var.min_ecs_task_percent}"
  max_ecs_task_percent = "${var.max_ecs_task_percent}"
  cloudwatch_log_group_name = "${module.cloudwatch_logs.name}"
}


module "docker-vm"
{
  source = "modules/docker-vm"

  deployment_name = "${var.deployment_name}"

  subnet_ids = "${module.network.subnet_ids}"
  default_security_group_id = "${module.network.default_security_group_id}"

  ssh_key_name = "${aws_key_pair.default.key_name}"

  data_bucket_arn = "${module.s3.data_bucket_arn}"
  landing_bucket_arn = "${module.s3.landing_bucket_arn}"

  ami_id = "${local.docker_ami_id}"
  bootstrap_with_ubuntu = "${var.bootstrap_docker_with_ubuntu}"
  deployment_tags = "${local.deployment_tags}"
  size =  "${var.docker_vm_size}"
  production = "${var.production}"
}


module "api_gateway" {
  source = "modules/api_gateway"
  deployment_name = "${var.deployment_name}"
  docker_image_url = "${join("", list(var.api_gateway_docker_image_url, ":", var.api_gateway_docker_image_tag))}"
  zone_id = "${var.zone_id}"
  endpoint_name = "${local.deployment_name}"
  cluster_id = "${module.ecs_cluster.cluster_id}"
  vpc_id = "${module.network.vpc_id}"
  aws_alb_arn = "${module.job_status_service.aws_alb_arn}"
  aws_alb_listener_arn = "${module.job_status_service.aws_alb_secure_listener_arn}"
  to_be_deployed = "${var.is_master}"
  deployment_tags = "${local.deployment_tags}"
  min_ecs_task_percent = "${var.min_ecs_task_percent}"
  max_ecs_task_percent = "${var.max_ecs_task_percent}"
  cloudwatch_log_group_name = "${module.cloudwatch_logs.name}"
}

module "acm" {
  source = "modules/acm"
  domain_name = "${local.clean_domain_name}"
  zone_id = "${var.zone_id}"
  region_name = "${local.endpoint_name}"
  deployment_tags = "${local.deployment_tags}"
  deployment_name = "${local.deployment_name}"
}

module "cloudwatch_logs" {
  source = "modules/cloudwatch_logs"
  deployment_tags = "${local.deployment_tags}"
  deployment_name = "${var.deployment_name}"
  cloudwatch_log_retention_in_days = "${var.cloudwatch_log_retention_in_days}"
}

module "rabbitmq" {
  source = "modules/rabbitmq"
  deployment_name = "${var.deployment_name}"
  rabbitmq_docker_image_url = "${var.rabbitmq_docker_image_url}"
  rabbitmq_docker_image_tag = "${var.rabbitmq_docker_image_tag}"
  zone_id = "${var.zone_id}"
  vpc_id = "${module.network.vpc_id}"
  aws_alb_arn = "${module.job_status_service.aws_alb_arn}"
  aws_alb_listener_arn = "${module.job_status_service.aws_alb_secure_listener_arn}"
  to_be_deployed = "${var.is_master}"
  deployment_tags = "${local.deployment_tags}"
  min_ecs_task_percent = "${var.min_ecs_task_percent}"
  max_ecs_task_percent = "${var.max_ecs_task_percent}"
  cloudwatch_log_group_name = "${module.cloudwatch_logs.name}"
  iam_instance_profile = "${module.ecs_cluster.iam_instance_profile}"
  subnet_ids = "${module.network.subnet_ids}"
  ssh_key_name = "${aws_key_pair.default.key_name}"
  ami_id = "${local.ecs_ami_id}"
  rabbit_username = "${var.rabbit_username}"
  rabbit_password = "${var.rabbit_password}"
  acm_certificate_arn = "${module.acm.acm_arn}"
  instance_type = "${var.instance_type}"
}
