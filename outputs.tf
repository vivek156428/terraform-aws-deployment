output "key_name"
{
  value = "${aws_key_pair.default.key_name}"
}

output "private_key"
{
  value = "${var.public_key != "" ? "" : tls_private_key.default.private_key_pem}"
}

output "acm_arn" {
  value = "${module.acm.acm_arn}"
}

output "cloudwatch_log_group_name" {
  value = "${module.cloudwatch_logs.name}"
}

output "houston_base_url" {
  value = "${local.clean_domain_name}"
}
