# terraform-aws-houston

Terraform to deploy Houston Environment. 
Keep in mind this environment needs to be on a different account from Storage Manager

What Houston does is it securely handles all the data and stores it encrypted and secure. This is practically the backend

### How to use this module

Don't run this module manually. Use the [deploy-houston](https://az.ey.tools/gitlab/project-orion/deploy-houston) repo in order to use this.

# Vars
| Name                          | Description                                                                                                                                                       | Type   | Default       | Required |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|---------------|----------|
| deployment_name               | the deployment name (used in object name tags, etc)                                                                                                               | string |               |          |
| chargecode                    | EY chargecode                                                                                                                                                     | string |               |          |
| consumer                      | Consumer(s) of the deployment (comma separated)                                                                                                                   | string |               |          |
| techcontacts                  | Technical contact for deployment issues                                                                                                                           | string |               |          |
| extra_tags                    | Map of extra tags to apply to every taggable object                                                                                                               | map    |               |          |
| zone_id                       | AWS domain ID of the Route53 domain to use for DNS records                                                                                                        | string |               |          |
| orion_url                     | the associated storage manager (orion) URL                                                                                                                        | string |               |          |
| ami_id                        | CID of reference AMI to use for non-ECS instances                                                                                                                 | string |               |          |
| ecs_ami_id                    | CID of reference AMI to use for ECS instances                                                                                                                     | string |               |          |
| docker_ami_id                 | CID of reference AMI to use for Docker instances                                                                                                                  | string |               |          |
| bootstrap_docker_with_ubuntu  | bootstrap dockervm with Ubuntu script else Amazon Linux script                                                                                                    | string |               |          |
| rds_database_name             | the RDS database name                                                                                                                                             | string | houston       |          |
| rds_username                  | the RDS database username                                                                                                                                         | string |               |          |
| rds_password                  | the RDS database password                                                                                                                                         | string |               |          |
| rds_instance_count            | the RDS instance count                                                                                                                                            | string | 1             |          |
| rds_instance_type             | the RDS instance type                                                                                                                                             | string | db.t2.medium  |          |
| subnet_count                  | number of subnets to create in new VPC or use (via output reference) in external VPC                                                                              | string | 2             |          |
| docker_instance_count         | number of Docker worker VMs to deploy                                                                                                                             | string | 1             |          |
| rabbit_username               | RabbitMQ username                                                                                                                                                 | string |               |          |
| rabbit_password               | RabbitMQ password                                                                                                                                                 | string |               |          |
| rabbit_host                   | RabbitMQ host                                                                                                                                                     | string |               |          |
| basic_auth_username           | Username for Houston's Basic Auth                                                                                                                                 | string |               |          |
| basic_auth_password           | Password for Houston's Basic Auth                                                                                                                                 | string |               |          |
| authorized_keys               | content of OpenSSH's authorized_keys for jumpbox                                                                                                                  | string |               |          |
| public_key                    | public key to use for deployment SSH key pair (default: auto generate)                                                                                            | string |               |          |
| data_bucket_sprintf           | C printf style syntax for generating the bucket name (single %s to receive the current region)                                                                    | string |               |          |
| landing_bucket_sprintf        | C printf style syntax for generating the bucket name (single %s to receive the current region)                                                                    | string |               |          |
| endpoint_name_sprintf         | C printf style syntax for generating the endpoint name (single %s to receive base name)                                                                           | string |               |          |
| ecr_region                    | region of ECR repos for Houston Workers docker images                                                                                                             | string |               |          |
| job_status_docker_image_url   | url where Job Status Service docker image can be found                                                                                                            | string |               |          |
| job_status_docker_image_tag   | Tag to use for Status Service docker image                                                                                                                        | string | latest        |          |
| job_runner_docker_image_url   | url where Job Runner docker image can be found                                                                                                                    | string |               |          |
| job_runner_docker_image_tag   | Tag to use for Job Runner docker image                                                                                                                            | string | latest        |          |
| db_name                       | database name                                                                                                                                                     | string | houston       |          |
| landing_bucket_prefix         | key prefix for the upload watcher Lambda trigger                                                                                                                  | string | landing_zone/ |          |
| is_master                     | Specify if you want to do a multi-region deployment with API Gateway, single regions don't use it. Empty means false, any string means true (even if it is false) | string |               |          |
| api_gateway_docker_image_url  | url where API Gateway docker image can be found                                                                                                                   | string |               |          |
| api_gateway_docker_image_tag  | Tag to use for API Gateway docker image                                                                                                                           | string | latest        |          |
| ey_houston_env_docker_url     | Url to use for Houston Env                                                                                                                                        | string |               |          |
| ey_houston_env_docker_tag     | Tag to use for Houston Env                                                                                                                                        | string | latest        |          |
| buckets_access_principles_arn | principle arn in SM account to grant access to landing and data buckets                                                                                           | string |               |          |
| upload_watcher_s3_bucket      | s3 bucket to use for lambda function upload watcher                                                                                                               | string |               |          |
| docker_vm_size                | size for the docker vm                                                                                                                                            | string | t2.large      |          |
| production                    | wether you are running prod or not, if true it will keep the ec2 volumes, s3 buckets and create rds final shapshot                                                | boolean| true          |          |