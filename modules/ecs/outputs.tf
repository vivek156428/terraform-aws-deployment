output "cluster_id" { value = "${aws_ecs_cluster.default.id}" }

output "instance_role_name" { value = "${aws_iam_role.instance.name}" }
output "service_role_name" { value = "${aws_iam_role.service.name}" }

output "instance_role_id" { value = "${aws_iam_role.instance.id}" }
output "service_role_id" { value = "${aws_iam_role.service.id}" }
output "iam_instance_profile" { value = "${aws_iam_instance_profile.instance.id}"}
