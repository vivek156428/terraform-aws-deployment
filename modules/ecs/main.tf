resource "aws_ecs_cluster" "default" {
  name = "${var.cluster_name}"
}


resource "aws_launch_configuration" "main" {
  name_prefix          = "${var.deployment_name}-ecs-launchconfig"
  image_id             = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  key_name = "${var.ssh_key_name}"
  iam_instance_profile = "${aws_iam_instance_profile.instance.id}"
  root_block_device {
    volume_size = "${var.block_size}"
    delete_on_termination = "${local.volume_termination_protection}"
  }
  user_data            =<<EOF
#!/bin/bash
echo ECS_CLUSTER="${aws_ecs_cluster.default.name}" >> /etc/ecs/ecs.config
EOF
}

resource "aws_autoscaling_group" "main" {
  name                 = "${var.deployment_name}-ecs-autoscaling"
//  vpc_zone_identifier  = ["${aws_subnet.main-public-1.id}", "${aws_subnet.main-public-2.id}"]
  vpc_zone_identifier  = ["${var.subnet_ids}"]
  launch_configuration = "${aws_launch_configuration.main.name}"
  min_size             = "${var.instance_count}"
  max_size             = "${var.instance_count}"

/*   tags = ["${merge(
    var.deployment_tags,
    map(
      "Name", "${var.cluster_name}-houston-ecs",
      "propagate_at_launch", true
    )
  )}"] */
}
