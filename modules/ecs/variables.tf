variable "deployment_name" {}
variable "cluster_name" {}
variable "instance_count" {}
variable "instance_type" {}
variable "subnet_ids" { type = "list" }
variable "block_size" {}
variable "ssh_key_name" {}
variable "ami_id" {}
variable "deployment_tags" { type = "map" }
variable "production" {}