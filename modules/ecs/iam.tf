resource "aws_iam_instance_profile" "instance" {
  name = "${var.cluster_name}-${data.aws_region.current.name}-instance"
  role = "${aws_iam_role.instance.name}"
}


resource "aws_iam_role" "instance" {
  name = "${var.cluster_name}-${data.aws_region.current.name}-instance"
  description = "ECS ${var.cluster_name} cluster instance role for the ${var.deployment_name} deployment."

  assume_role_policy = <<EOF
{
"Version": "2008-10-17",
"Statement": [
  {
    "Sid": "",
    "Effect": "Allow",
    "Principal": {
      "Service": "ec2.amazonaws.com"
    },
    "Action": "sts:AssumeRole"
  }
]
}
EOF
}


resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerServiceforEC2Role" {
  role = "${aws_iam_role.instance.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}


resource "aws_iam_role_policy_attachment" "CloudWatchLogsFullAccess" {
  role = "${aws_iam_role.instance.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}


resource "aws_iam_instance_profile" "service" {
  name = "${var.cluster_name}-${data.aws_region.current.name}-service"
  role = "${aws_iam_role.service.name}"
}


resource "aws_iam_role" "service" {
  name = "${var.cluster_name}-${data.aws_region.current.name}-service"
  description = "ECS ${var.cluster_name} cluster service role for the ${var.deployment_name} deployment."

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_role_policy" "service" {
  name = "${var.cluster_name}-${data.aws_region.current.name}-backend-service"
  role = "${aws_iam_role.service.id}"

  policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Effect": "Allow",
    "Action": [
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:Describe*",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:Describe*",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
      "elasticloadbalancing:RegisterTargets"
    ],
    "Resource": "*"
  }
]
}
EOF
}
