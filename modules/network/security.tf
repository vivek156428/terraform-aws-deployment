resource "aws_default_security_group" "default"
{
  vpc_id = "${aws_vpc.default.id}"

  ingress
  {
    self = true
    protocol = -1
    from_port = 0
    to_port = 0
  }

  egress
  {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = -1
    from_port = 0
    to_port = 0
  }

    tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}-local-all",
      "Description", "Allow all security group local traffic"
    )
  )}"
}


resource "aws_security_group" "public-ssh"
{
  vpc_id = "${aws_vpc.default.id}"
  name = "houston-${var.deployment_name}-public-ssh"
  description = "Allow incomming SSH from Internet"

  ingress
  {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "tcp"
    from_port = 22
    to_port = 22
  }
  ingress
  {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "tcp"
    from_port = 5555
    to_port = 5555
 }
}
