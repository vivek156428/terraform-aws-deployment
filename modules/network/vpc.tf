data "aws_availability_zones" "available" {}


resource "aws_vpc" "default"
{
  cidr_block = "172.16.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = false

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}"
    )
  )}"
}


resource "aws_subnet" "default"
{
  count = "${var.subnet_count}"

  vpc_id = "${aws_vpc.default.id}"
  availability_zone = "${element(data.aws_availability_zones.available.names, count.index)}"
  cidr_block = "172.16.${count.index}.0/24"
  map_public_ip_on_launch = true

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}-${count.index}"
    )
  )}"
}


resource "aws_internet_gateway" "default"
{
  vpc_id = "${aws_vpc.default.id}"

    tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}"
    )
  )}"
}


resource "aws_default_route_table" "default"
{
  default_route_table_id = "${aws_vpc.default.default_route_table_id}"

  route
  {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default.id}"
  }

    tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}"
    )
  )}"
}
