output "vpc_id"
{
  description = "AWS VPC CID"
  value = "${aws_vpc.default.id}"
}


output "subnet_ids"
{
  description = "list of CIDs for subnets defined for the new VPC"
  value = "${aws_subnet.default.*.id}"
}


output "default_security_group_id"
{
  description = "the default security group id"
  value = "${aws_vpc.default.default_security_group_id}"
}
output "jumpbox_ip"
{
  value = "${aws_instance.jumpbox.public_ip}"
}
