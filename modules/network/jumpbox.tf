resource "aws_route53_record" "jumpbox" {
  zone_id = "${var.zone_id}"
  name = "jumpbox.${var.deployment_name}"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.jumpbox.public_ip}"]
}


resource "aws_instance" "jumpbox" {
  ami = "${var.ami_id}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.default.*.id[0]}"
  private_ip = "172.16.0.4"  # the first available address in AWS subnets
  associate_public_ip_address = true

  key_name = "${var.ssh_key_name}"

  vpc_security_group_ids = [
    "${aws_vpc.default.default_security_group_id}",
    "${aws_security_group.public-ssh.id}"
  ]

  root_block_device = {
    delete_on_termination = "${local.volume_termination_protection}"
  }

  volume_tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}-jumpbox"
    )
  )}"

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}-jumpbox",
      "ACCESSED-VIA-INTERNET", "YES"
    )
  )}"

  user_data = <<EOL
#!/usr/bin/env bash
# This script implements initial configuration via the
# AWS user_data field


# Make sure that we are provisioned with the latest packages
sudo apt-get update
sudo apt-get install -y mysql-client


# Create devops group and associated sudoers rule
groupadd devops
cat > /etc/sudoers.d/devops << EOF
# User rules for devops group
%devops ALL=(ALL) NOPASSWD:ALL
EOF


# Add users allowed to SSH into jumpbox
while read username groups pubkey
do
  if [[ "$${groups}" == "-" ]]
  then
    groups=""
  else
    groups="-G $${groups}"
  fi

  useradd -s /bin/bash -m $${groups} "$${username}"
  guid=$(getent passwd $${username} | cut -d: -f4)
  dir=$(getent passwd $${username} | cut -d: -f6)

  fn="$${dir}/.ssh"
  mkdir "$${fn}"
  chown $${username}:$${guid} "$${fn}"
  chmod 0700 "$${fn}"

  fn="$${fn}/authorized_keys"
  echo -nE "$${pubkey}" > "$${fn}"
  chown "$${username}" "$${fn}"
  chmod 0600 "$${fn}"
done << 'EOF'
${var.authorized_keys}
EOF
EOL
}
