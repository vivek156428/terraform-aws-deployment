variable "deployment_name" {}
variable "zone_id" {}

variable "subnet_count" {}

variable "authorized_keys" {}
variable "ssh_key_name" {}

variable "ami_id" {}
variable "deployment_tags" { type = "map" }
variable "production" {}