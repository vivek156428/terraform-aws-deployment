data "aws_region" "current" {}

resource "aws_iam_role_policy_attachment" "ecr" {
  role       = "${var.instance_role_name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "ecs" {
  role       = "${var.instance_role_name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerServiceFullAccess"
}

resource "aws_ecs_service" "job_runner" {
  name                               = "${var.deployment_name}-job_runner"
  cluster                            = "${var.cluster_id}"
  task_definition                    = "${aws_ecs_task_definition.job_runner.arn}"
  deployment_maximum_percent         = "${var.max_ecs_task_percent}"
  deployment_minimum_healthy_percent = "${var.min_ecs_task_percent}"
  desired_count                      = "${var.desired_count}"
}

resource "aws_ecs_task_definition" "job_runner" {
  family       = "houston-${var.deployment_name}-job_runner"
  network_mode = "bridge"

  container_definitions = <<EOF
[
    {
      "dnsSearchDomains": null,
      "logConfiguration": null,
      "entryPoint": null,
      "portMappings": [
        {
          "containerPort": 5555;
          "protocol": "tcp"
        }
      ],
      "command": [
          "${data.aws_region.current.name}-${var.deployment_name}"
      ],
      "linuxParameters": null,
      "logConfiguration": {
      "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${var.cloudwatch_log_group_name}",
          "awslogs-region": "${data.aws_region.current.name}",
          "awslogs-stream-prefix": "${var.deployment_name}-job_runner"
        }
      },
      "cpu": 0,
      "environment": [
        {
          "name": "AWS_DEFAULT_REGION",
          "value": "${data.aws_region.current.name}"
        },
        {
          "name": "QUEUE",
          "value": "${data.aws_region.current.name}-${var.deployment_name}"
        },
        {
          "name": "DB_PASSWORD",
          "value": "${var.rds_password}"
        },
        {
          "name": "DB_SERVER",
          "value": "${var.rds_url}"
        },
        {
          "name": "DB_USERNAME",
          "value": "${var.rds_username}"
        },
        {
          "name": "DB_NAME",
          "value": "${var.rds_database_name}"
        },
        {
          "name": "DOCKER_HOST",
          "value": "${var.docker_host}"
        },
        {
          "name": "RABBIT_HOST",
          "value": "${var.rabbit_host}"
        },
        {
          "name": "RABBIT_PSWD",
          "value": "${var.rabbit_password}"
        },
        {
          "name": "RABBIT_USER",
          "value": "${var.rabbit_username}"
        },
        {
          "name": "ECR_REGION",
          "value": "${var.ecr_region}"
        },
        {
          "name": "TASK_ENV",
          "value": "{\"SM_URL\": \"${var.orion_url}\"}"
        },
        {
          "name": "WSGI_ENV",
          "value": "production"
        }
      ],
      "ulimits": null,
      "dnsServers": null,
      "mountPoints": [],
      "workingDirectory": null,
      "dockerSecurityOptions": null,
      "memory": 1024,
      "memoryReservation": null,
      "volumesFrom": [],
      "image": "${var.docker_image_url}",
      "disableNetworking": null,
      "essential": true,
      "links": null,
      "hostname": null,
      "extraHosts": null,
      "user": null,
      "readonlyRootFilesystem": null,
      "dockerLabels": null,
      "privileged": null,
      "name": "ey_houston_job_runner"
    }
]
EOF
}
