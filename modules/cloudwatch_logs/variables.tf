variable "deployment_name" {}

variable "deployment_tags" { type = "map" }

variable "cloudwatch_log_retention_in_days" {}
