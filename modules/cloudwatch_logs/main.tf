resource "aws_cloudwatch_log_group" "main" {
  name = "${var.deployment_name}-houston-logs"
  retention_in_days = "${var.cloudwatch_log_retention_in_days}"

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}"
    )
  )}"
}