output "arn" {
  value = "${aws_cloudwatch_log_group.main.arn}"
}

output "name" {
  value = "${var.deployment_name}-houston-logs"
}

