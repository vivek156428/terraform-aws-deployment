variable "deployment_name" {}
variable "zone_id" {}
variable "cluster_id" {}
variable "acm_certificate_arn" {}
variable "desired_count" {}
variable "service_role_id" {}
variable "rds_url" {}
variable "rds_database_name" {}
variable "rds_username" {}
variable "rds_password" {}
variable "subnet_ids" {type = "list" }
variable "default_security_group_id" {}
variable "vpc_id" {}
variable "rabbit_username" {}
variable "rabbit_password" {}
variable "rabbit_host" {}
variable "basic_auth_username" {}
variable "basic_auth_password" {}
variable "docker_image_url" {}
variable "deployment_tags" { type = "map" }
variable "endpoint_name" {}
variable "env_docker_repo" {}
variable "min_ecs_task_percent" {}
variable "max_ecs_task_percent" {}
variable "cloudwatch_log_group_name" {}
variable "db_on_restart" {}
