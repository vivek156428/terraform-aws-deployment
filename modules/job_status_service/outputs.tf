output "endpoint_dns_name" { value = "${aws_route53_record.endpoint.fqdn}" }
output "endpoint_url" { value = "https://${aws_route53_record.endpoint.fqdn}" }
output "aws_alb_arn" { value = "${aws_alb.endpoint.arn}" }
output "aws_alb_secure_listener_arn" { value = "${aws_alb_listener.secure.arn}" }
