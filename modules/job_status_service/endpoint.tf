resource "aws_route53_record" "endpoint" {
  zone_id = "${var.zone_id}"
  name = "${var.endpoint_name}"
  type = "CNAME"
  ttl = 60
  records = ["${aws_alb.endpoint.dns_name}"]
}


resource "aws_security_group" "public-https"
{
  vpc_id = "${var.vpc_id}"

  ingress
  {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "tcp"
    from_port = 443
    to_port = 443
  }
  ingress
  {
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "tcp"
    from_port = 80
    to_port = 80
  }
}


resource "aws_alb" "endpoint" {
  name = "houston-${var.deployment_name}-endpoint"
  subnets = ["${var.subnet_ids}"]
  security_groups = [
    "${var.default_security_group_id}",
    "${aws_security_group.public-https.id}"
  ]

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "${var.deployment_name}-endpoint"
    )
  )}"
}


resource "aws_alb_listener" "secure" {
  load_balancer_arn = "${aws_alb.endpoint.arn}"
  port = 443
  protocol = "HTTPS"

  certificate_arn = "${var.acm_certificate_arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.default.arn}"
    type = "forward"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = "${aws_alb.endpoint.arn}"
  port = 80
  protocol = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.default.arn}"
    type = "forward"
  }
}

resource "aws_alb_target_group" "default" {
  name = "${var.deployment_name}-default"
  port = 80
  protocol = "HTTP"
  vpc_id = "${var.vpc_id}"

  health_check {
    interval = "30"
    healthy_threshold = 2
    unhealthy_threshold = 8
  }


  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "${var.deployment_name}-default"
    )
  )}"
}
