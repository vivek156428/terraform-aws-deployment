data "aws_region" "current" {}


resource "aws_ecs_service" "job_status" {
  name = "${var.deployment_name}-job_status"
  cluster = "${var.cluster_id}"
  task_definition = "${aws_ecs_task_definition.job_status.arn}"

  desired_count = "${var.desired_count}"
  iam_role = "${var.service_role_id}"

  deployment_maximum_percent = "${var.max_ecs_task_percent}"
  deployment_minimum_healthy_percent = "${var.min_ecs_task_percent}"
  load_balancer {
    target_group_arn = "${aws_alb_target_group.default.arn}"
    container_name =  "ey_job_status_service"
    container_port = 80
  }

  depends_on = ["aws_alb_target_group.default"]
}


resource "aws_ecs_task_definition" "job_status" {
  family = "houston-${var.deployment_name}-job_status"
  network_mode = "bridge"

  container_definitions = <<EOF
[
  {
    "dnsSearchDomains": null,
    "logConfiguration": null,
    "entryPoint": null,
    "portMappings": [
      {
        "hostPort": 80,
        "protocol": "tcp",
        "containerPort": 80
      }
    ],
    "command": null,
    "linuxParameters": null,
    "logConfiguration": {
      "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${var.cloudwatch_log_group_name}",
          "awslogs-region": "${data.aws_region.current.name}",
          "awslogs-stream-prefix": "${var.deployment_name}-job_status"
        }
      },
    "cpu": 0,
    "environment": [
      {
          "name": "AWS_DEFAULT_REGION",
          "value": "${data.aws_region.current.name}"
      },
      {
        "name": "QUEUE",
        "value": "${data.aws_region.current.name}-${var.deployment_name}"
      },
      {
        "name": "DB_PASSWORD",
        "value": "${var.rds_password}"
      },
      {
        "name": "DB_SERVER",
        "value": "${var.rds_url}"
      },
      {
          "name": "DB_NAME",
          "value": "${var.rds_database_name}"
      },
      {
        "name": "DB_USERNAME",
        "value": "${var.rds_username}"
      },
      {
        "name": "RABBIT_HOST",
        "value": "${var.rabbit_host}"
      },
      {
        "name": "RABBIT_PSWD",
        "value": "${var.rabbit_password}"
      },
      {
        "name": "RABBIT_USER",
        "value": "${var.rabbit_username}"
      },
      {
        "name": "BASIC_AUTH_USERNAME",
        "value": "${var.basic_auth_username}"
      },
      {
        "name": "BASIC_AUTH_PASSWORD",
        "value": "${var.basic_auth_password}"
      },
      {
        "name": "ENV_DOCKER_REPO",
        "value": "${var.env_docker_repo}"
      },
      {
        "name": "DB_ON_RESTART",
        "value": "${var.db_on_restart}"
      }
    ],
    "ulimits": null,
    "dnsServers": null,
    "mountPoints": [],
    "workingDirectory": null,
    "dockerSecurityOptions": null,
    "memory": 1024,
    "memoryReservation": null,
    "volumesFrom": [],
    "image": "${var.docker_image_url}",
    "disableNetworking": null,
    "essential": true,
    "links": null,
    "hostname": null,
    "extraHosts": null,
    "user": null,
    "readonlyRootFilesystem": null,
    "dockerLabels": null,
    "privileged": null,
    "name": "ey_job_status_service"
  }
]
EOF
}
