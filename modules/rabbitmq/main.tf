data "template_file" "rabbitmq-task-definition-template" {
  count = "${local.count}"
  template               = "${file("${path.module}/rabbitmq.json.tpl")}"
  vars {
    RABBIT_REPOSITORY_URL = "${local.docker_image_url}"
    USERNAME = "${var.rabbit_username}"
    PASSWORD = "${var.rabbit_password}"
    AWS_DEFAULT_REGION = "${data.aws_region.current.name}"
    CLOUDWATCH_LOG_GROUP_NAME = "${var.cloudwatch_log_group_name}"
    DEPLOYMENT_NAME = "${var.deployment_name}"
  }
}

# Create Security group for ELB
resource "aws_security_group" "rabbit" {
  count = "${local.count}"
  name        = "rabbit"
  description = "Allow Rabbitmq traffic"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 5672
    to_port     = 5672
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "queue-${var.deployment_name}"
    )
  )}"
}


resource "aws_ecs_task_definition" "rabbitmq-task-definition" {
  count = "${local.count}"
  family                = "houston-${var.deployment_name}-rabbitmq"
  container_definitions = "${data.template_file.rabbitmq-task-definition-template.rendered}"
}


resource "aws_ecs_service" "rabbitmq" {
  count = "${local.count}"
  name = "houston-queue-${var.deployment_name}"
  cluster = "${aws_ecs_cluster.default.id}"
  task_definition = "${aws_ecs_task_definition.rabbitmq-task-definition.arn}"
  desired_count = 1
  deployment_maximum_percent = "${var.max_ecs_task_percent}"
  deployment_minimum_healthy_percent = "${var.min_ecs_task_percent}"
  load_balancer
 {
    elb_name = "${aws_elb.rabbitmq-elb.name}"
    container_name = "rabbit"
    container_port = 5672
//    container_port = 15672
  }
}
