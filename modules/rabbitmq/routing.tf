resource "aws_route53_record" "rabbitmq-record" {
  count = "${local.count}"
  zone_id = "${var.zone_id}"
  name    = "${local.rabbit_subdomain}"
  type    = "CNAME"
  ttl     = "5"

  weighted_routing_policy {
    weight = 90
  }

  set_identifier = "rabbit1.dev"
  records        = ["${aws_elb.rabbitmq-elb.dns_name}"]
}



resource "aws_elb" "rabbitmq-elb" {
  count = "${local.count}"
  name = "queue-${var.deployment_name}"

  listener {
    instance_port = 5672
    instance_protocol = "tcp"
    lb_port = 5672
    lb_protocol = "tcp"
  }
  listener {
    instance_port = 15672
    instance_protocol = "http"
    ssl_certificate_id = "${var.acm_certificate_arn}"
    lb_port = 15672
    lb_protocol = "https"
  }
  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 3
    timeout = 30
    target = "HTTP:15672/"
    interval = 60
  }
  cross_zone_load_balancing = true
  idle_timeout = 400
  connection_draining = true
  connection_draining_timeout = 30

  subnets = ["${var.subnet_ids}"]
  security_groups = ["${aws_security_group.rabbit.id}"]
  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "queue-${var.deployment_name}"
    )
  )}"
}
# Enable Proxy mode for ELB (need for getting the correct client IPs)
