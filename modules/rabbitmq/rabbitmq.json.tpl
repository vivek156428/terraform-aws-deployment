[
  {
    "essential": true,
    "memory": 512,
    "name": "rabbit",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${CLOUDWATCH_LOG_GROUP_NAME}",
        "awslogs-region": "${AWS_DEFAULT_REGION}",
        "awslogs-stream-prefix": "${DEPLOYMENT_NAME}-rabbitmq"
      }
    },
    "cpu": 512,
    "image": "${RABBIT_REPOSITORY_URL}",
    "Environment": [
        {"name": "RABBITMQ_ERL_COOKIE", "value": "drecyotgtxtymt"},
        {"name": "RABBITMQ_DEFAULT_USER", "value": "${USERNAME}"},
        {"name": "RABBITMQ_DEFAULT_PASS", "value": "${PASSWORD}"}
    ],
    "portMappings": [
        {
            "containerPort": 5671,
            "hostPort": 5671
        },
        {
            "containerPort": 5672,
            "hostPort": 5672
        },
        {
            "containerPort": 4369,
            "hostPort": 4369
        },
        {
            "containerPort": 15672,
            "hostPort": 15672
        },
        {
            "containerPort": 15671,
            "hostPort": 15671
        },
        {
            "containerPort": 25672,
            "hostPort": 25672
        }
    ]
  }
]
