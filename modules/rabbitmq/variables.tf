variable "cloudwatch_log_group_name" {}
variable "zone_id" {}
variable "deployment_name" {}
variable "deployment_tags" { type = "map" }
variable "min_ecs_task_percent" {}
variable "max_ecs_task_percent" {}
variable "acm_certificate_arn" {}
variable "to_be_deployed" {}
variable "rabbit_username" {}
variable "rabbit_password" {}
variable "aws_alb_arn" {}
variable "subnet_ids" { type = "list" }
variable "ssh_key_name" {}
variable "ami_id" {}
variable "rabbitmq_docker_image_tag" {}
variable "rabbitmq_docker_image_url" {}
variable "instance_type" {}
variable "iam_instance_profile" {}
variable "aws_alb_listener_arn" {}
variable "vpc_id" {}



