resource "aws_ecs_cluster" "default" {
  count = "${local.count}"
  name = "houston-queue-${var.deployment_name}"
}

resource "aws_launch_configuration" "main" {
  count = "${local.count}"
  name_prefix          = "${var.deployment_name}-queue-launchconfig"
  image_id             = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  key_name = "${var.ssh_key_name}"
  iam_instance_profile = "${var.iam_instance_profile}"
  security_groups = ["${aws_security_group.rabbit.id}"]
  lifecycle {
    create_before_destroy = true
  }
  root_block_device {
    volume_size = 8
  }
  user_data            =<<EOF
#!/bin/bash
echo ECS_CLUSTER="${aws_ecs_cluster.default.name}" >> /etc/ecs/ecs.config
EOF
}

resource "aws_autoscaling_group" "main" {
  count = "${local.count}"
  name                 = "${var.deployment_name}-queue-autoscaling"
  vpc_zone_identifier  = ["${var.subnet_ids}"]
  launch_configuration = "${aws_launch_configuration.main.name}"
  min_size             = 1
  max_size             = 1
  lifecycle {
    create_before_destroy = true
  }
/*   tag = "${merge(
    var.deployment_tags,
    map(
      "Name", "${var.cluster_name}-houston-ecs"
    )
  )}" */
}
