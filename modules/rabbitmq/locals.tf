locals {
  count = "${var.to_be_deployed != "" ? 1 : 0}"

  rabbit_subdomain = "${var.deployment_name != "default" ? join("-", list(var.deployment_name, "queue")) : "queue" }"

  docker_image_url = "${join("", list(var.rabbitmq_docker_image_url, ":", var.rabbitmq_docker_image_tag))}"
}
