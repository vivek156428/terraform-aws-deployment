data "aws_s3_bucket" "bucket" {
  bucket = "${var.landing_bucket_id}"
}


resource "aws_iam_role" "iam_for_lambda" {
  name = "${var.deployment_name}-${data.aws_region.current.name}-upload-watcher-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_region" "current"
{
  current = true
}
data "aws_caller_identity" "current" {}

resource "aws_iam_policy" "lambda" {
  name = "${var.deployment_name}-${data.aws_region.current.name}-upload-watcher-policy"
  description = "Upload Watcher Lambda policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetObject",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "upload-watcher" {
  role       = "${aws_iam_role.iam_for_lambda.name}"
  policy_arn = "${aws_iam_policy.lambda.arn}"
}

resource "aws_lambda_function" "upload_watcher" {
  s3_bucket = "${local.s3_bucket}"
  s3_key = "upload_watcher-production.zip"
  function_name    = "${var.deployment_name}-upload-watcher"
  role             = "${aws_iam_role.iam_for_lambda.arn}"
  handler          = "index.handler"
  runtime          = "nodejs6.10"

  environment {
    variables = "${var.env_vars}"
  }

  tags = "${var.deployment_tags}"
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.upload_watcher.arn}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${data.aws_s3_bucket.bucket.arn}"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${var.landing_bucket_id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.upload_watcher.arn}"
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "${var.landing_bucket_prefix}"
  }
}
