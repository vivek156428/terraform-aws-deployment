locals {
  s3_bucket = "${var.s3_bucket == "" ? "${var.deployment_name}-houston-lambdas-${data.aws_region.current.name}" :
                var.s3_bucket
                }"
}
