variable "deployment_name" {}
variable "landing_bucket_id" {}
variable "landing_bucket_prefix" {}

variable "s3_bucket" {}

variable "env_vars" { type = "map" }

variable "deployment_tags" { type = "map" }
variable "cloudwatch_log_group_name" {}