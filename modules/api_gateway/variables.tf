variable "deployment_name" {}
variable "docker_image_url" {}
variable "zone_id" {}
variable "endpoint_name" {}
variable "cluster_id" {}
variable "vpc_id" {}
variable "aws_alb_arn" {}
variable "aws_alb_listener_arn" {}
variable "deployment_tags" {
  type = "map"
}
variable "to_be_deployed" {}
variable "min_ecs_task_percent" {}
variable "max_ecs_task_percent" {}
variable "cloudwatch_log_group_name" {}
