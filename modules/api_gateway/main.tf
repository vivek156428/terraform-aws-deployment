data "aws_region" "current" {}

resource "aws_ecs_service" "api_gateway" {
  count = "${local.count}"
  name = "${var.deployment_name}-api_gateway"
  cluster = "${var.cluster_id}"
  task_definition = "${aws_ecs_task_definition.api_gateway.arn}"

  desired_count = 1
  deployment_maximum_percent = "${var.max_ecs_task_percent}"
  deployment_minimum_healthy_percent = "${var.min_ecs_task_percent}" 
  load_balancer {
    target_group_arn = "${aws_alb_target_group.default.arn}"
    container_name =  "ey_houston_api_gateway"
    container_port = 80
  }

  depends_on = ["aws_alb_target_group.default"]
}

data "aws_route53_zone" "default" {
  zone_id = "${var.zone_id}"
}

resource "aws_ecs_task_definition" "api_gateway" {
  count = "${local.count}"
  family = "houston-${var.deployment_name}-api_gateway"
  network_mode = "bridge"

  container_definitions = <<EOF
[
    {
      "dnsSearchDomains": null,
      "logConfiguration": null,
      "entryPoint": null,
      "portMappings": [
        {
          "hostPort": 81,
          "protocol": "tcp",
          "containerPort": 80
        }
      ],
      "command": null,
      "linuxParameters": null,
      "logConfiguration": {
      "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${var.cloudwatch_log_group_name}",
          "awslogs-region": "${data.aws_region.current.name}",
          "awslogs-stream-prefix": "${var.deployment_name}-api_gateway"
        }
      },
      "cpu": 0,
      "environment": [
        {
          "name": "DEPLOYMENT_NAME",
          "value": "${var.endpoint_name}"
        },
        {
          "name": "SUBDOMAIN",
          "value": "${substr(data.aws_route53_zone.default.name, 0, length(data.aws_route53_zone.default.name) - 1)}"
        }
      ],
      "ulimits": null,
      "dnsServers": null,
      "mountPoints": [],
      "workingDirectory": null,
      "dockerSecurityOptions": null,
      "memory": 1024,
      "memoryReservation": null,
      "volumesFrom": [],
      "image": "${var.docker_image_url}",
      "disableNetworking": null,
      "essential": true,
      "links": null,
      "hostname": null,
      "extraHosts": null,
      "user": null,
      "readonlyRootFilesystem": null,
      "dockerLabels": null,
      "privileged": null,
      "name": "ey_houston_api_gateway"
    }
]
EOF
}
