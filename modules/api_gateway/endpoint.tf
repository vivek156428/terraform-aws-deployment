data "aws_alb" "default" {
  arn = "${var.aws_alb_arn}"
}

resource "aws_route53_record" "endpoint" {
  count   = "${local.count}"
  zone_id = "${var.zone_id}"
  name    = "${var.endpoint_name}"
  type    = "A"

  alias {
    name                   = "${data.aws_alb.default.dns_name}"
    zone_id                = "${data.aws_alb.default.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_lb_listener_rule" "api-gateway" {
  count        = "${local.count}"
  listener_arn = "${var.aws_alb_listener_arn}"
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.default.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${aws_route53_record.endpoint.fqdn}"]
  }
}

resource "aws_alb_target_group" "default" {
  count             = "${local.count}"
  name              = "${var.deployment_name}-gateway"
  instance_port     = 5555
  instance_protocol = "tcp"
  port              = 81
  protocol          = "HTTP"
  vpc_id            = "${var.vpc_id}"

  health_check {
    interval            = "30"
    healthy_threshold   = 2
    unhealthy_threshold = 8
  }

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "${var.deployment_name}-gateway"
    )
  )}"
}
