resource "aws_s3_bucket" "data" {
  bucket = "${var.data_bucket_name}"

  cors_rule {
    allowed_headers = ["Authorization"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    expose_headers = ["ETag"]
    max_age_seconds = 3000
  }
  force_destroy = "${local.force_s3_destroy}"
  tags = "${var.deployment_tags}"
}


resource "aws_s3_bucket_policy" "data" {
  bucket = "${aws_s3_bucket.data.id}"
  policy = <<END
{
  "Version": "2008-10-17",
  "Statement": [
      {
          "Sid": "${var.data_bucket_name}-allow",
          "Effect": "Allow",
          "Principal": {
              "AWS": "${var.principle_arn}"
          },
          "Action": [
              "s3:PutObject",
              "s3:PutObjectAcl",
              "s3:ListBucket"
          ],
          "Resource": [
              "${aws_s3_bucket.data.arn}",
              "${aws_s3_bucket.data.arn}/*"
          ]
      },
      {
          "Sid": "${var.data_bucket_name}-deny",
          "Effect": "Deny",
          "Principal": {
              "AWS": "${var.principle_arn}"
          },
          "Action": [
              "s3:GetObject"
          ],
          "Resource": [
              "${aws_s3_bucket.data.arn}",
              "${aws_s3_bucket.data.arn}/*"
          ]
      }
  ]
}
END
}

