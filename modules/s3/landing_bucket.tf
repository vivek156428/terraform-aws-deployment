resource "aws_s3_bucket" "landing" {
  bucket = "${var.landing_bucket_name}"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_methods = ["PUT"]
    allowed_methods = ["POST"]
    allowed_origins = ["*"]
    expose_headers = ["ETag"]
    max_age_seconds = 3000
  }
  force_destroy = "${local.force_s3_destroy}"
  tags = "${var.deployment_tags}"

  lifecycle_rule {
    enabled = true
    id = "export"
    prefix = "_CraneExport/"

    expiration {
      days = "${var.expiration_days_for_export}"
    }
  }

  lifecycle_rule {
    enabled = true
    id = "import"
    prefix = "landing_zone/"

    expiration {
      days = "${var.expiration_days_for_import}"
    }
  }
}


resource "aws_s3_bucket_policy" "landing" {
  bucket = "${aws_s3_bucket.landing.id}"
  policy = <<END
{
  "Version": "2008-10-17",
  "Statement": [
      {
          "Sid": "${var.landing_bucket_name}-allow",
          "Effect": "Allow",
          "Principal": {
              "AWS": "${var.principle_arn}"
          },
          "Action": [
              "s3:PutObject",
              "s3:PutObjectAcl",
              "s3:ListBucket"
          ],
          "Resource": [
              "${aws_s3_bucket.landing.arn}",
              "${aws_s3_bucket.landing.arn}/*"
          ]
      },
      {
          "Sid": "${var.landing_bucket_name}-deny",
          "Effect": "Deny",
          "Principal": {
              "AWS": "${var.principle_arn}"
          },
          "Action": [
              "s3:GetObject"
          ],
          "Resource": [
              "${aws_s3_bucket.landing.arn}",
              "${aws_s3_bucket.landing.arn}/*"
          ]
      }
  ]
}
END
}

