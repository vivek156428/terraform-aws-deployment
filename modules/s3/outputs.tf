output "data_bucket_id" { value = "${aws_s3_bucket.data.id}" }
output "landing_bucket_id" { value = "${aws_s3_bucket.landing.id}" }

output "data_bucket_arn" { value = "${aws_s3_bucket.data.arn}" }
output "landing_bucket_arn" { value = "${aws_s3_bucket.landing.arn}" }
