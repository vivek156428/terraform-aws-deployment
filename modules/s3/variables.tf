variable "deployment_name" {}
variable "data_bucket_name" {}
variable "landing_bucket_name" {}
variable "principle_arn" {}

variable "deployment_tags" { type = "map" }

variable "expiration_days_for_export" { default = 7 }
variable "expiration_days_for_import" { default = 1 }
variable "production" {}