variable "deployment_name" {}
variable "zone_id" {}
variable "subnet_ids" { type = "list" }

variable "rds_database_name" {}
variable "rds_username" {}
variable "rds_password" {}
variable "rds_instance_count" {}
variable "rds_instance_type" {}

variable "security_group_ids" { type = "list" }
variable "deployment_tags" { type = "map" }
variable "production" {}