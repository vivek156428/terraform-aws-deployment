resource "aws_route53_record" "rds" {
  zone_id = "${var.zone_id}"
  name = "rds.${var.deployment_name}"
  type = "CNAME"
  ttl = "60"
  records = ["${aws_rds_cluster.houston.endpoint}"]
}


resource "aws_db_subnet_group" "houston" {
  name = "houston-${var.deployment_name}-vpc"
  subnet_ids = ["${var.subnet_ids}"]

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}"
    )
  )}"
}


resource "aws_rds_cluster" "houston" {
  cluster_identifier = "houstons-${var.deployment_name}"
  master_username = "${var.rds_username}"
  master_password = "${var.rds_password}"

  db_subnet_group_name = "${aws_db_subnet_group.houston.name}"
  vpc_security_group_ids = ["${var.security_group_ids}"]

  database_name = "${var.rds_database_name}"

  skip_final_snapshot = "${local.skip_final_snapshot}"
  final_snapshot_identifier = "${var.deployment_name}-final-snapshot"
}


resource "aws_rds_cluster_instance" "houston_instances" {
  cluster_identifier = "${aws_rds_cluster.houston.id}"
  count = "${var.rds_instance_count}"
  identifier = "houston-${var.deployment_name}-${count.index}"
  instance_class = "${var.rds_instance_type}"
  db_subnet_group_name = "${aws_db_subnet_group.houston.name}"
}
