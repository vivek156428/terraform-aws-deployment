resource "aws_acm_certificate" "houston" {
  domain_name = "${var.domain_name}"
  subject_alternative_names = ["${join("", list(var.region_name, ".", var.domain_name))}", "${join("", list("*.", var.domain_name))}"]
  validation_method = "DNS"
  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}"
    )
  )}"
}

resource "aws_acm_certificate_validation" "houston" {
  certificate_arn = "${aws_acm_certificate.houston.arn}"
  validation_record_fqdns = ["${aws_route53_record.houston-acm.*.fqdn}"]
}


resource "aws_route53_record" "houston-acm" {
    count = 3
    name = "${lookup(aws_acm_certificate.houston.domain_validation_options[count.index], "resource_record_name")}"
    type = "${lookup(aws_acm_certificate.houston.domain_validation_options[count.index], "resource_record_type")}"
    zone_id = "${var.zone_id}"
    records = ["${lookup(aws_acm_certificate.houston.domain_validation_options[count.index], "resource_record_value")}"]
    ttl     = 60
}