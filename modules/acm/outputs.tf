output "acm_arn" {
  value = "${aws_acm_certificate.houston.arn}"
}

output "domain_validation_options" {
  value = "${aws_acm_certificate.houston.domain_validation_options}"
}

