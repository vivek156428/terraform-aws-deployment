resource "aws_instance" "docker-vm"
{
  count = "1"

  subnet_id = "${element(var.subnet_ids, count.index)}"
  instance_type = "${var.size}"

  key_name = "${var.ssh_key_name}"
  iam_instance_profile = "${aws_iam_instance_profile.dockervm.id}"

  tags = "${merge(
    var.deployment_tags,
    map(
      "Name", "houston-${var.deployment_name}-runner-engine-${count.index}"
    )
  )}"

  ami = "${var.ami_id}"
  user_data = "${var.bootstrap_with_ubuntu == ""
    ? file("${path.module}/bootstrap.bash")
    : file("${path.module}/ubuntu_bootstrap.bash")
  }"

  volume_tags = "${merge(
      var.deployment_tags,
      map(
        "Name", "houston-${var.deployment_name}-runner-engine"
      )
  )}"

  root_block_device = {
    volume_size = 40
    delete_on_termination = "${local.volume_termination_protection}"
  }
}
