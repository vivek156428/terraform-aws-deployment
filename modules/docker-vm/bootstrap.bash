#!/usr/bin/env bash
if ! command -v docker
then
  yum install -y docker
  chkconfig docker on
  service docker start
  usernod -a -G docker ec2-user
fi


curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get --yes update
sudo apt-get --yes install docker-ce
echo 'Starting docker daemon'
sudo systemctl stop docker
sudo systemctl start docker
sudo mkdir /etc/systemd/system/docker.service.d

touch /tmp/hosts.conf
cat <<EOT >> /tmp/hosts.conf
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2375
EOT

sudo mv /tmp/hosts.conf /etc/systemd/system/docker.service.d/hosts.conf

sudo systemctl daemon-reload
sudo systemctl restart docker
