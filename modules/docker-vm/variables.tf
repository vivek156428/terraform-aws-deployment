variable "deployment_name" {}

variable "subnet_ids" { type = "list" }
variable "default_security_group_id" {}

variable "ssh_key_name" {}

variable "data_bucket_arn" {}
variable "landing_bucket_arn" {}

variable "ami_id" {}
variable "bootstrap_with_ubuntu" {}
variable "deployment_tags" { type = "map" }

variable "size" {}

variable "production" {}
