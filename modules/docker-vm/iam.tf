resource "aws_iam_instance_profile" "dockervm" {
  name = "houst-${var.deployment_name}-${data.aws_region.current.name}-dockervm"
  role = "${aws_iam_role.dockervm.name}"
}

resource "aws_iam_role" "dockervm" {
  name = "houston-${var.deployment_name}-${data.aws_region.current.name}-dockervm"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "CloudWatchLogsFullAccess" {
  role = "${aws_iam_role.dockervm.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}


resource "aws_iam_role_policy" "S3-access" {
  name = "houston-${var.deployment_name}-${data.aws_region.current.name}-dockervm"
  role = "${aws_iam_role.dockervm.id}"

  policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Effect": "Allow",
    "Action": [
      "s3:Get*",
      "s3:Put*",
      "s3:List*"
    ],
    "Resource": [
      "${var.data_bucket_arn}/*",
      "${var.landing_bucket_arn}/*"
    ]
  },
  {
    "Effect": "Allow",
    "Action": [
      "s3:List*"
    ],
    "Resource": [
      "${var.data_bucket_arn}",
      "${var.landing_bucket_arn}"
    ]
  }
]
}
EOF
}
