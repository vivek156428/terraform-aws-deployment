output "docker_vm_address"
{
  value = "${aws_instance.docker-vm.private_ip}:2375"
}