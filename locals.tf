locals
{
  deployment_name = "${var.deployment_name == "default" ? "" : var.deployment_name }"
  clean_domain_name = "${replace(data.aws_route53_zone.houston.name, "/[.]$/", "")}"
  deployment_tags = "${merge(
    var.extra_tags,
    map(
      "APP-NAME", "houston",
      "DEPLOYMENT-NAME", var.deployment_name,
      "CHARGECODE", var.chargecode,
      "CONSUMER", var.consumer,
      "TECHCONTACTS", var.techcontacts,
      "ACCESSED-VIA-INTERNET", "NO"
    )
  )}"

  # Ensure that we have acceptible default AMI CIDs
  ami_id = "${var.ami_id != "" ? var.ami_id : data.aws_ami.ami.image_id }"
  ecs_ami_id = "${var.ecs_ami_id != "" ? var.ecs_ami_id : data.aws_ami.ecs_ami.image_id }" 
  docker_ami_id = "${var.docker_ami_id != "" ? var.docker_ami_id : data.aws_ami.ubuntu.image_id }"

  # Create reasonable default bucket sprintf values
  data_bucket_sprintf = "${var.data_bucket_sprintf != ""
    ? var.data_bucket_sprintf
    : "orion-${var.deployment_name}-data-%s"
  }"
  data_bucket_name = "${format(local.data_bucket_sprintf, data.aws_region.current.name)}"

  landing_bucket_sprintf = "${var.landing_bucket_sprintf != ""
    ? var.landing_bucket_sprintf
    : "orion-${var.deployment_name}-landing-%s"
  }"
  landing_bucket_name = "${format(local.landing_bucket_sprintf, data.aws_region.current.name)}"

  endpoint_name_sprintf = "${var.endpoint_name_sprintf != "" ? var.endpoint_name_sprintf : 
                             var.deployment_name == "default" ? local.deployment_name : 
                             "${var.deployment_name}"
  }"
  endpoint_name = "${var.deployment_name != "default" ? join("-", list(local.endpoint_name_sprintf, data.aws_region.current.name)) :
                    data.aws_region.current.name  
                  }"

  rabbit_host = "${var.deployment_name == "default" ? join(".", list("queue", local.clean_domain_name)) : 
              join(".", list(join("-", list(local.deployment_name, "queue")), local.clean_domain_name))}"
              
  orion_url = "${var.deployment_name == "default" ? 
                    var.is_master == "true" ?
                        var.orion_base_url
                        : join(".", list(data.aws_region.current.name, var.orion_base_url))
                    : var.is_master == "true" ?
                        join(".", list(local.deployment_name, var.orion_base_url))
                        : join(".", list(data.aws_region.current.name, local.deployment_name, var.orion_base_url))
              }"
}
