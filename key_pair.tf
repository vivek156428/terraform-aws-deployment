resource "tls_private_key" "default"
{
  algorithm = "RSA"
  rsa_bits = 2048
}


resource "aws_key_pair" "default"
{
  key_name = "houston-${var.deployment_name}"
  public_key = "${var.public_key != "" ? var.public_key : tls_private_key.default.public_key_openssh}"
}
